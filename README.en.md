# 数据中台

#### Description
一站式数据开发及管理“数据中台”，是由多个组
成的数据管理应用平台，是一款实用敏捷的数据全生命周期  。聚焦于大数据采集、交换、处理、存储、分析、管控、 数据开发等数据处理及数据应用全环节，涵盖企业数据业务  的全链路解决方案，包含离线和实时数据集成、数据治理、  数据安全、数据服务、数据资产管理相关平台及工具以及数  据标准体系建设方法论，可以助力企业数字化转型，为不同  行业用户构建数据资产，实现数据价值变现且持续增值。


#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
